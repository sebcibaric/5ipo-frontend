import ax from 'axios'

var axios = ax.create({
    baseURL: 'http://localhost:8080/api',
}, function logout() {
    axios.defaults.headers['Authorization'] = ''
    localStorage.removeItem('token')
});



export default axios