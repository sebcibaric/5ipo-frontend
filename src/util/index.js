
function removeById(arr, id) {
    let index;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].id === id)
            index = i;
    }

    arr.splice(index, 1)

    return arr;
}

function findIndexById(arr, id) {
    let index;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].id == id) {
            index = i;
        }
    }

    return index;
}

export { removeById, findIndexById };
