import Vue from 'vue'
import Router from 'vue-router'
import Store from '../views/Store'
import Product from '../views/Product'
import Category from '../views/Category'
import ProductStore from '../views/ProductStore'
import Login from '../views/Login'
import Role from '../views/Role'
import User from '../views/User'
import MyProfile from '../views/MyProfile'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'ProductStore',
      component: ProductStore,
      meta: { authorize: ['ADMIN', 'STORE_ADMIN', 'USER'] }
    },
    {
      path: '/store',
      name: 'Store',
      component: Store,
      meta: { authorize: ['ADMIN'] }
    },
    {
      path: '/product',
      name: 'Product',
      component: Product,
      meta: { authorize: ['ADMIN'] }
    },
    {
      path: '/category',
      name: 'Category',
      component: Category,
      meta: { authorize: ['ADMIN'] }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/role',
      name: 'Role',
      component: Role,
      meta: { authorize: ['ADMIN'] }
    },
    {
      path: '/user',
      name: 'User',
      component: User,
      meta: { authorize: ['ADMIN'] }
    },
    {
      path: '/myProfile',
      name: 'MyProfile',
      component: MyProfile,
      meta: { authorize: ['ADMIN', 'STORE_ADMIN', 'USER'] }
    }
  ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const { authorize } = to.meta;
    const user = router.app.$store.state.user;

    if (authorize) {
        if (!user) {
            // not logged in so redirect to login page with the return url
            return next({ path: '/login', query: { returnUrl: to.path } });
        }

        // check if route is restricted by role
        if (authorize.length && !authorize.includes(user.roleName)) {
            // role not authorised so redirect to home page
            return next({ path: '/' });
        }
    }

    next();
})

export default router;