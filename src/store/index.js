import Vue from 'vue'
import Vuex from 'vuex'
import axios from '../auth'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
	state: {
		status: '',
		isAdmin: false,
		isStoreAdmin: false,
		isUser: false,
		token: localStorage.getItem('token') || '',
		user : null
	},
	mutations: {
		auth_request(state){
			state.status = 'loading'
		},
		auth_success(state, user){
			state.status = 'success'
			state.token = user.token
			state.user = user
			if (user.roleName === 'ADMIN') {
				state.isAdmin = true;
			} else if (user.roleName === 'STORE_ADMIN') {
				state.isStoreAdmin = true;
			} else if (user.roleName === 'USER') {
				state.isUser = true;
			}
		},
		auth_error(state){
		  state.status = 'error';
		},
		logout(state){
			state.status = '';
			state.token = '';
			state.user = null;
			state.isAdmin = false;
			state.isStoreAdmin = false;
			state.isUser = false;
		},
	},
	actions: {
		login({commit}, user) {
			return new Promise((resolve, reject) => {
				commit('auth_request')
				axios.post('/users/login', user)
				.then(resp => {
					var user = {
						id: resp.data.data.id,
						username: resp.data.data.username,
						firstName: resp.data.data.firstName,
						lastName: resp.data.data.lastName,
						roleId: resp.data.data.roleId,
						roleName: resp.data.data.roleName,
						storeId: resp.data.data.storeId,
						storeName: resp.data.data.storeName,
						token: 'Bearer ' + resp.data.data.token
					};
					
					localStorage.setItem('token', user.token);
					axios.defaults.headers.common['Authorization'] = user.token;
					commit('auth_success', user);
					resolve(resp);
				})
				.catch(err => {
					commit('auth_error')
					localStorage.removeItem('token')
					reject(err)
				})
			})
		},

		logout({commit}) {
			return new Promise((resolve, reject) => {
				commit('logout')
				localStorage.removeItem('token')
				axios.defaults.headers.common['Authorization'] = ''
				let removed = true
				resolve(removed)
			})
		}
	},
	getters : {
		isLoggedIn: state => !!state.token,
		authStatus: state => state.status,
		isAdmin: state => state.isAdmin,
		isStoreAdmin: state => state.isStoreAdmin,
		isUser: state => state.isUser
	}
})
